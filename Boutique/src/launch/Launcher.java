package launch;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import view_modele.BoutiqueVM;

public class Launcher extends Application {

    private static BoutiqueVM boutiqueVM;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("/fxml/MainWindow.fxml"));
        Scene scene = new Scene(parent);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static BoutiqueVM getBoutiqueVM() {
        if(boutiqueVM == null){
            boutiqueVM = new BoutiqueVM();
        }
        return boutiqueVM;
    }

    @Override
    public void stop() throws Exception {
        boutiqueVM.sauver();
        super.stop();
    }
}
