package Modele.metier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Parfum extends Produit{

    private List<String> fragrances;

    public static final String PROP_AJOUT_FRAGRANCE = "ajout_fragrance";

    public static final String PROP_SUPPRESSION_FRAGRANCE = "suppression_fragrance";

    public Parfum(String nom, int prix) {
        super(nom, prix);
        fragrances = new ArrayList<>();
    }

    public Parfum(String nom, int prix, List<String> fragrances) {
        this(nom, prix);
        this.fragrances = fragrances;
    }

    public void ajouterFragrance(String fragrance){
        fragrances.add(fragrance);
        int index = fragrances.size() - 1;
        getSupport().fireIndexedPropertyChange(PROP_AJOUT_FRAGRANCE, index, null, fragrance);
    }

    public void supprimerFragrance(String fragrance){
        fragrances.remove(fragrance);
        getSupport().firePropertyChange(PROP_SUPPRESSION_FRAGRANCE, fragrance, null);
    }

    public List<String> getFragrances(){
        return Collections.unmodifiableList(fragrances);
    }


}
