package Modele.metier;

import java.io.Serializable;

public class Couleur implements Serializable {

    private int vert;

    private int rouge;

    private int bleue;

    public Couleur(int vert, int rouge, int bleue) {
        this.vert = vert;
        this.rouge = rouge;
        this.bleue = bleue;
    }

    public int getVert() {
        return vert;
    }

    public int getRouge() {
        return rouge;
    }

    public int getBleue() {
        return bleue;
    }

    @Override
    public boolean equals(Object obj){
        if(obj == null) return false;
        if(this == obj) return true;
        if(getClass() != obj.getClass()) return false;

        Couleur other = (Couleur)obj;
        return vert == other.getVert() && rouge == other.getRouge() && bleue == other.getBleue();
    }
}
