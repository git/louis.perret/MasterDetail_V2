package Modele.metier;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Boutique implements Serializable {

    private List<Produit> produits;

    transient private PropertyChangeSupport support = new PropertyChangeSupport(this);

    public static final String PROP_AJOUT = "AjoutProduit";
    public static final String PROP_SUPPRESSION = "SuppressionProduit";

    public Boutique(){
        produits = new ArrayList<>();
    }

    public Boutique(List<Produit> produits) {
        this.produits = produits;
    }

    public void addListener(PropertyChangeListener listener){
        getSupport().addPropertyChangeListener(listener);
    }

    public void ajouterProduit(Produit produit){
        produits.add(produit);
    }

    public void ajouterHabit(String nom, int prix){
        Produit produit = new Habit(nom, prix);
        produits.add(produit);
        int index = produits.size() - 1;
        getSupport().fireIndexedPropertyChange(PROP_AJOUT, index, null, produit);
    }

    public void ajouterParfum(String nom, int prix){
        Produit produit = new Parfum(nom, prix);
        produits.add(produit);
        int index = produits.size() - 1;
        getSupport().fireIndexedPropertyChange(PROP_AJOUT, index, null, produit);
    }

    public void supprimerProduit(Produit produit){
        produits.remove(produit);
        getSupport().firePropertyChange(PROP_SUPPRESSION, produit, null);
    }

    public List<Produit> getProduits() {
        return Collections.unmodifiableList(produits);
    }

    protected PropertyChangeSupport getSupport() {
        if(support == null){
            support = new PropertyChangeSupport(this);
        }
        return support;
    }
}
