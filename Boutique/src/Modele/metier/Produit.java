package Modele.metier;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Objects;

public abstract class Produit implements Serializable {

    private String nom;

    private int prix;

    transient protected PropertyChangeSupport support = new PropertyChangeSupport(this);

    protected Produit(String nom, int prix){
        this.nom = nom;
        this.prix = prix;
    }

    public void addListener(PropertyChangeListener listener){
        getSupport().addPropertyChangeListener(listener);
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    protected PropertyChangeSupport getSupport() {
        if(support == null){
            support = new PropertyChangeSupport(this);
        }
        return support;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null) return false;
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Produit produit = (Produit) o;
        return prix == produit.prix && nom.equals(produit.nom);
    }
}
