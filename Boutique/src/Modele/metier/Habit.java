package Modele.metier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Habit extends Produit{

    private List<Taille> taillesEnStock;

    private List<Couleur> couleurs;

    public static final String PROP_ADD_COULEUR = "ajout_couleur";
    public static final String PROP_REMOVE_COULEUR = "suppression_couleur";

    public static final String PROP_ADD_TAILLE = "ajout_taille";

    public static final String PROP_REMOVE_TAILLE = "suppression_taille";

    public Habit(String nom, int prix) {
        super(nom, prix);
        taillesEnStock = new ArrayList<>();
        couleurs = new ArrayList<>();
    }

    public void ajouterTaille(Taille taille){
        taillesEnStock.add(taille);
        int index = taillesEnStock.size() - 1;
        getSupport().fireIndexedPropertyChange(PROP_ADD_TAILLE, index, null, taille);
    }

    public void ajouterCouleur(int vert, int rouge, int bleue){
        Couleur couleur = new Couleur(vert, rouge, bleue);
        couleurs.add(couleur);
        int index = couleurs.size() - 1;
        getSupport().fireIndexedPropertyChange(PROP_ADD_COULEUR, index, null, couleur);
    }

    public void supprimerTaille(Taille taille){
        taillesEnStock.remove(taille);
        getSupport().firePropertyChange(PROP_REMOVE_TAILLE, taille, null);
    }

    public void supprimerCouleur(Couleur couleur){
        couleurs.remove(couleur);
        getSupport().firePropertyChange(PROP_REMOVE_COULEUR, couleur, null);
    }

    public List<Taille> getTaillesEnStock() {
        return Collections.unmodifiableList(taillesEnStock);
    }

    public List<Couleur> getCouleurs() {
        return Collections.unmodifiableList(couleurs);
    }
}
