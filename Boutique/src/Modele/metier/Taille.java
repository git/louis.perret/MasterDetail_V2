package Modele.metier;

import javax.sql.rowset.serial.SerialJavaObject;
import java.io.Serializable;

public enum Taille implements Serializable {

    XS,
    S,
    M,
    L,
    XL
}
