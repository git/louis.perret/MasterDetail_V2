package data.chargement;

import Modele.metier.Boutique;
import Modele.metier.Produit;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface Chargeur {

    public Boutique charger(String nomFichier) throws IOException, ClassNotFoundException;
}
