package data.chargement;

import Modele.metier.Boutique;
import Modele.metier.Produit;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface Sauveur {

    public void sauver(Boutique produits, String nomFichier) throws IOException;
}
