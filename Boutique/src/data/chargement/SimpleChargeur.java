package data.chargement;

import Modele.metier.Boutique;
import Modele.metier.Produit;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class SimpleChargeur implements Chargeur{

    @Override
    public Boutique charger(String nomFichier) throws IOException, ClassNotFoundException {
        Boutique produits;
        try(ObjectInputStream reader = new ObjectInputStream(new FileInputStream(nomFichier))){
            produits = (Boutique) reader.readObject();
        }
        return produits;
    }
}
