package data.chargement;

import Modele.metier.*;

import java.util.ArrayList;
import java.util.List;

public class Stub implements Chargeur{


    @Override
    public Boutique charger(String nomFichier) {
        Boutique boutique = new Boutique();

        Habit p1 = new Habit("Echarpe", 42);
        p1.ajouterTaille(Taille.XL);
        p1.ajouterTaille(Taille.L);
        p1.ajouterTaille(Taille.XS);
        p1.ajouterCouleur(0,220,120);
        p1.ajouterCouleur(50,0,5);

        Habit p2 = new Habit("Gants", 15);
        p2.ajouterTaille(Taille.XL);

        Habit p3 = new Habit("Chemise", 22);
        p3.ajouterCouleur(0,255,0);

        Habit p4 = new Habit("Chaussure", 100);
        p4.ajouterTaille(Taille.S);
        p4.ajouterTaille(Taille.M);

        Habit p5 = new Habit("Chaussettes", 450);
        p5.ajouterTaille(Taille.XL);
        p5.ajouterTaille(Taille.XL);

        Parfum p6 = new Parfum("Insolence", 80);
        p6.ajouterFragrance("Rose");

        Parfum p7 = new Parfum("J'adore Dior", 100);
        p7.ajouterFragrance("Peche");

        Parfum p8 = new Parfum("Scorpion", 120);
        p8.ajouterFragrance("Marron");

        Parfum p9 = new Parfum("La petire robe noire", 75);
        p9.ajouterFragrance("Paille");

        Parfum p10 = new Parfum("Scandal", 99);
        p10.ajouterFragrance("Vache");

        boutique.ajouterProduit(p1);
        boutique.ajouterProduit(p2);
        boutique.ajouterProduit(p3);
        boutique.ajouterProduit(p4);
        boutique.ajouterProduit(p5);
        boutique.ajouterProduit(p6);
        boutique.ajouterProduit(p7);
        boutique.ajouterProduit(p8);
        boutique.ajouterProduit(p9);
        boutique.ajouterProduit(p10);

        return boutique;
    }
}
