package data.chargement;

import Modele.metier.Boutique;
import Modele.metier.Produit;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

public class SimpleSauveur implements Sauveur{

    @Override
    public void sauver(Boutique produits, String nomFichier) throws IOException {
        try(ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(nomFichier))){
            writer.writeObject(produits);
        }
    }
}
