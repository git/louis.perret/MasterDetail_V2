package view_modele;

import Modele.metier.Taille;

import java.util.ArrayList;
import java.util.List;

public class TailleVMTailleConverter {

    public static Taille toTaille(TailleVM taille){
        Taille ans = null;
        switch (taille){
            case XS -> ans = Taille.XS;
            case S -> ans = Taille.S;
            case M -> ans = Taille.M;
            case L -> ans = Taille.L;
            case XL -> ans = Taille.XL;
        }

        return ans;
    }

    public static List<Taille> toTailles(List<TailleVM> tailles){
        List<Taille> ans = new ArrayList<>();
        for(var taille : tailles){
            ans.add(toTaille(taille));
        }

        return ans;
    }

    public static TailleVM toTailleVM(Taille taille){
        TailleVM ans = null;
        switch (taille){
            case XS -> ans = TailleVM.XS;
            case S -> ans = TailleVM.S;
            case M -> ans = TailleVM.M;
            case L -> ans = TailleVM.L;
            case XL -> ans = TailleVM.XL;
        }

        return ans;
    }

    public static List<TailleVM> toTaillesVM(List<Taille> tailles){
        List<TailleVM> ans = new ArrayList<>();
        for(var taille : tailles){
            ans.add(toTailleVM(taille));
        }

        return ans;
    }
}
