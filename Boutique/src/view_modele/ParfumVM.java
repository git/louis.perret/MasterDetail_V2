package view_modele;

import Modele.metier.Habit;
import Modele.metier.Parfum;
import Modele.metier.Produit;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.beans.IndexedPropertyChangeEvent;
import java.beans.PropertyChangeEvent;
import java.util.List;

public class ParfumVM extends ProduitVM {

    private ObservableList<String> fragrancesObs = FXCollections.observableArrayList();
    private ListProperty<String> listeFragrances = new SimpleListProperty<>(fragrancesObs);
    public ObservableList<String> getListeFragrances() { return listeFragrances.get(); }
    public ListProperty<String> listeFragrancesProperty() { return listeFragrances; }
    public void setListeFragrances(ObservableList<String> listeFragrances) { this.listeFragrances.set(listeFragrances); }

    public ParfumVM(String nom, int prix, List<String> fragrances){
        this(new Parfum(nom, prix, fragrances));
    }

    public ParfumVM(Parfum produit){
        super(produit);
        fragrancesObs.addAll(produit.getFragrances());
    }

    public void addFragrance(String fragrance){
        ((Parfum)modele).ajouterFragrance(fragrance);
    }

    public void removeFragrance(String fragrance){
        ((Parfum)modele).supprimerFragrance(fragrance);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()){
            case Parfum.PROP_AJOUT_FRAGRANCE -> fragrancesObs.add(((IndexedPropertyChangeEvent)evt).getIndex(), (String)evt.getNewValue());
            case Parfum.PROP_SUPPRESSION_FRAGRANCE -> fragrancesObs.remove((String)evt.getOldValue());
        }
    }

    @Override
    public boolean equals(Object obj){
        return super.equals(obj);
    }
}
