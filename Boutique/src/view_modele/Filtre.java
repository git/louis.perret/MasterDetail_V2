package view_modele;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public enum Filtre {

    TOUS,
    HABIT,
    PARFUM;

    public static ObservableList<Filtre> getFiltres(){
        ObservableList<Filtre> ans = FXCollections.observableArrayList();
        for(var filtre : Filtre.values()){
            ans.add(filtre);
        }

        return ans;
    }

}
