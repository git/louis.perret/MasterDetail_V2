package view_modele;

import Modele.metier.Boutique;
import Modele.metier.Habit;
import Modele.metier.Parfum;
import Modele.metier.Produit;
import data.chargement.*;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;

import java.beans.IndexedPropertyChangeEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.List;

public class BoutiqueVM implements PropertyChangeListener {

    private Boutique modele;

    private static final String NOMFICHIER = "source.bin";
    private Chargeur chargeur;

    private Sauveur sauveur;

    private ObservableList<ProduitVM> produitsObs = FXCollections.observableArrayList();

    private ListProperty<ProduitVM> listeProduits = new SimpleListProperty<>(produitsObs);
    public ObservableList<ProduitVM> getListeProduits() { return FXCollections.unmodifiableObservableList(listeProduits.get()); }
    public ReadOnlyListProperty<ProduitVM> listeProduitsProperty() { return listeProduits; }
    public void setListeProduits(ObservableList<ProduitVM> listeProduits) { this.listeProduits.set(listeProduits); }

    private FilteredList<ProduitVM> listeFiltre = new FilteredList<>(produitsObs, p -> true);

    public FilteredList<ProduitVM> getListeFiltre() {
        return listeFiltre;
    }

    public BoutiqueVM(){
        chargeur = new SimpleChargeur();
        sauveur = new SimpleSauveur();
        try {
            modele = chargeur.charger(NOMFICHIER);
        } catch (Exception e) {
            modele = new Stub().charger(NOMFICHIER);
        }

        initialiserListe();
        modele.addListener(this);
    }

    public void initialiserListe(){
        for(Produit p : modele.getProduits()){
            produitsObs.add(creerProduitVM(p));
        }
    }

    private ProduitVM creerProduitVM(Produit p){
        ProduitVM produit = null;

        if(p instanceof Habit habit){
            produit = new HabitVM(habit);
        }
        else if(p instanceof Parfum parfum){
            produit = new ParfumVM(parfum);
        }

        return produit;
    }

    public void ajouterParfum(String nom, int prix){
        modele.ajouterParfum(nom, prix);
    }

    public void ajouterHabit(String nom, int prix){
        modele.ajouterHabit(nom, prix);
    }

    public void supprimerProduit(ProduitVM produit){
        modele.supprimerProduit(produit.getModele());
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()){
            case Boutique.PROP_AJOUT:
                produitsObs.add(((IndexedPropertyChangeEvent)evt).getIndex(), creerProduitVM((Produit)evt.getNewValue()));
                break;
            case Boutique.PROP_SUPPRESSION:
                produitsObs.remove(creerProduitVM((Produit)evt.getOldValue()));
                break;
        }
    }

    public void sauver() throws IOException {
        sauveur.sauver(modele, NOMFICHIER);
    }

    public void filtrer(Filtre filtre){
        switch (filtre){
            case TOUS -> listeFiltre.setPredicate(p -> true);
            case HABIT -> listeFiltre.setPredicate(p -> p instanceof HabitVM);
            case PARFUM -> listeFiltre.setPredicate(p -> p instanceof ParfumVM);
        }
    }
}
