package view_modele;

import Modele.metier.Couleur;
import Modele.metier.Habit;
import Modele.metier.Produit;
import Modele.metier.Taille;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.beans.IndexedPropertyChangeEvent;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;

public class HabitVM extends ProduitVM {

    private ObservableList<CouleurVM> couleurObs = FXCollections.observableArrayList();

    private ListProperty<CouleurVM> listeCouleurs = new SimpleListProperty<>(couleurObs);

    public ObservableList<CouleurVM> getListeCouleurs() { return listeCouleurs.get(); }
    public ListProperty<CouleurVM> listeCouleursProperty() { return listeCouleurs; }
    public void setListeCouleurs(ObservableList<CouleurVM> listeCouleurs) { this.listeCouleurs.set(listeCouleurs); }


    private ObservableList<TailleVM> taillesObs = FXCollections.observableArrayList();

    private ListProperty<TailleVM> listeTailles = new SimpleListProperty<>(taillesObs);

    public ObservableList<TailleVM> getListeTailles() { return listeTailles.get(); }
    public ListProperty<TailleVM> listeTaillesProperty() { return listeTailles; }
    public void setListeTailles(ObservableList<TailleVM> listeTailles) { this.listeTailles.set(listeTailles); }

    public HabitVM(String nom, int prix){
        this(new Habit(nom, prix));

    }

    public HabitVM(Habit produit){
        super(produit);
        for(Couleur c : ((Habit)modele).getCouleurs()){
            couleurObs.add(new CouleurVM(c));
        }
        taillesObs.addAll(TailleVMTailleConverter.toTaillesVM(((Habit) modele).getTaillesEnStock()));
    }

    public void ajouterCouleur(int vert, int rouge, int bleue){
        ((Habit)modele).ajouterCouleur(vert, rouge, bleue);
    }

    public void supprimerCouleur(CouleurVM couleur){
        ((Habit)modele).supprimerCouleur(couleur.getModele());
    }

    public void ajouterTaille(TailleVM taille){
        ((Habit)modele).ajouterTaille(TailleVMTailleConverter.toTaille(taille));
    }

    public void supprimerTaille(TailleVM taille){
        ((Habit)modele).supprimerTaille(TailleVMTailleConverter.toTaille(taille));
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()){
            case Habit.PROP_ADD_COULEUR -> couleurObs.add(((IndexedPropertyChangeEvent)evt).getIndex(), new CouleurVM((Couleur)evt.getNewValue()));
            case Habit.PROP_REMOVE_COULEUR -> couleurObs.remove((new CouleurVM((Couleur)evt.getOldValue())));
            case Habit.PROP_ADD_TAILLE ->  taillesObs.add(((IndexedPropertyChangeEvent)evt).getIndex(),TailleVMTailleConverter.toTailleVM((Taille)evt.getNewValue()));
            case Habit.PROP_REMOVE_TAILLE -> taillesObs.remove(TailleVMTailleConverter.toTailleVM((Taille)evt.getOldValue()));
        }
    }

    public ObservableList<TailleVM> getToutesLesTailles(){
        ObservableList<TailleVM> ans =FXCollections.observableArrayList();
        ans.add(TailleVM.XS);
        ans.add(TailleVM.S);
        ans.add(TailleVM.M);
        ans.add(TailleVM.L);;
        ans.add(TailleVM.XL);

        return ans;
    }

    @Override
    public boolean equals(Object obj){
        return super.equals(obj);
    }

}
