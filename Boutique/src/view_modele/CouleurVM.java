package view_modele;

import Modele.metier.Couleur;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

import javax.swing.plaf.IconUIResource;

public class CouleurVM {

    private Couleur modele;

    private IntegerProperty  vert = new SimpleIntegerProperty();
    public int getVert() { return vert.get(); }
    public IntegerProperty vertProperty() { return vert; }
    public void setVert(int vert) { this.vert.set(vert); }

    private IntegerProperty  rouge = new SimpleIntegerProperty();
    public int getRouge() { return rouge.get(); }
    public IntegerProperty rougeProperty() { return rouge; }
    public void setRouge(int rouge) { this.rouge.set(rouge); }

    private IntegerProperty bleue = new SimpleIntegerProperty();
    public int getBleue() { return bleue.get(); }
    public IntegerProperty bleueProperty() { return bleue; }
    public void setBleue(int bleue) { this.bleue.set(bleue); }

    public CouleurVM(Couleur modele) {
        this.modele = modele;
        setVert(modele.getVert());
        setRouge(modele.getRouge());
        setBleue(modele.getBleue());
    }

    public CouleurVM(int vert, int rouge, int bleue){
        this(new Couleur(vert, rouge, bleue));
    }

    public Couleur getModele() {
        return modele;
    }

    @Override
    public boolean equals(Object obj){
        if(obj == null) return false;
        if(this == obj) return true;
        if(getClass() != obj.getClass()) return false;

        CouleurVM other = (CouleurVM)obj;
        return modele.equals(other.modele);
    }
}
