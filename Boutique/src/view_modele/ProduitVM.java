package view_modele;

import Modele.metier.Produit;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.beans.PropertyChangeListener;

public abstract class ProduitVM implements PropertyChangeListener {

    protected Produit modele;

    protected StringProperty nom = new SimpleStringProperty();
    public String getNom() { return nom.get(); }
    public StringProperty nomProperty() { return nom; }
    public void setNom(String nom) { this.nom.set(nom); }

    protected IntegerProperty prix = new SimpleIntegerProperty();
    public int getPrix() { return prix.get(); }
    public IntegerProperty prixProperty() { return prix; }
    public void setPrix(int prix) { this.prix.set(prix); }

    protected ProduitVM(Produit produit){
        this.modele = produit;
        setNom(modele.getNom());
        setPrix(modele.getPrix());

        nom.addListener(__ -> this.modele.setNom(getNom()));
        prix.addListener(__ -> this.modele.setPrix(getPrix()));

        this.modele.addListener(this);
    }

    public Produit getModele() {
        return modele;
    }

    @Override
    public boolean equals(Object obj){
        if(obj == null) return false;
        if(this == obj) return true;
        if(getClass() != obj.getClass()) return false;

        ProduitVM other = (ProduitVM) obj;
        return modele.equals(other.getModele());
    }
}
