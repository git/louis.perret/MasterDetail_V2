package view_modele;

public enum TailleVM {

    XS,
    S,
    M,
    L,
    XL
}
