package view;

import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.util.ArrayList;
import java.util.List;

public class AjoutHabit {

    @FXML
    private TextField nomHabit;

    @FXML
    private TextField prixHabit;


    private boolean isCanceled = false;

    @FXML
    private void ajouterHabit() {
        fermerFenetre();
    }

    @FXML
    private void annuler() {
        isCanceled = true;
        fermerFenetre();
    }

    private void fermerFenetre(){
        nomHabit.getScene().getWindow().hide();
    }


    public String getNomHabit(){
        return nomHabit.getText();
    }

    public int getPrixHabit(){
        return Integer.parseInt(prixHabit.getText());
    }


    public boolean isCanceled() {
        return isCanceled;
    }

}
