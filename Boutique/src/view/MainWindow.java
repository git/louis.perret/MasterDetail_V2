package view;

import Modele.metier.Habit;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;
import javafx.util.converter.NumberStringConverter;
import launch.Launcher;
import view.cellules.CelluleProduit;
import view.uc.UCDetailHabit;
import view.uc.UCDetailParfum;
import view_modele.*;

import java.io.IOException;
import java.util.Arrays;

public class MainWindow {

    @FXML
    private VBox layoutDetail;

    @FXML
    private ComboBox listeFiltrage;

    @FXML
    private ListView<ProduitVM> listeProduit;

    private BoutiqueVM boutiqueVM = Launcher.getBoutiqueVM();

    private UCDetailParfum detailParfum;

    private UCDetailHabit detailHabit;

    public void initialize(){
        try {
            detailParfum = new UCDetailParfum();
            detailParfum.setVisibiliy(false);
            layoutDetail.getChildren().add(detailParfum);
            detailHabit = new UCDetailHabit();
            detailHabit.setVisibiliy(false);
            layoutDetail.getChildren().add(detailHabit);
        } catch (IOException e) {
            new Alert(Alert.AlertType.ERROR, "Erreur", ButtonType.OK).show();
        }

        listeFiltrage.setItems(Filtre.getFiltres());
        listeFiltrage.getSelectionModel().selectedItemProperty().addListener(__ -> setFiltre());
        listeProduit.setItems(boutiqueVM.getListeFiltre());
        listeProduit.setCellFactory((__) -> new CelluleProduit());
        listeProduit.getSelectionModel().selectedItemProperty().addListener((__, oldV, newV) -> {
            if(oldV != null){
                if(oldV instanceof ParfumVM){
                    detailParfum.setVisibiliy(false);
                }
                else if(oldV instanceof HabitVM){
                    detailHabit.setVisibiliy(false);
                }
            }
            if(newV != null) {
                if(newV instanceof ParfumVM parfumVM){
                    detailParfum.setVisibiliy(true);
                    detailParfum.setParfumVM(parfumVM);
                }
                else if(newV instanceof HabitVM habitVM){
                    detailHabit.setVisibiliy(true);
                    detailHabit.setHabitVM(habitVM);
                }
            }
        });
    }

    private void setFiltre() {
        boutiqueVM.filtrer((Filtre)listeFiltrage.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void ajouterHabit(){
        boutiqueVM.ajouterHabit("Nouvel habit", 0);
    }

    @FXML
    private void ajouterParfum(){
        boutiqueVM.ajouterParfum("Nouvel parfum", 0);
    }

   /* @FXML
    private void lancerFenetreAjoutParfum(){
        Stage secondStage = new Stage();
        secondStage.initOwner(listeProduit.getScene().getWindow());
        secondStage.initModality(Modality.WINDOW_MODAL);
        AjoutParfum controlleur = initAjouterParfum(secondStage);
        if(!controlleur.isCanceled()){
            boutiqueVM.ajouterParfum(controlleur.getNomParfum(), controlleur.getPrixParfum(), controlleur.getFragrancesParfum());
        }
    }

    private AjoutParfum initAjouterParfum(Stage stageAjoutParfum) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Ajout_Parfum.fxml"));
        AjoutParfum controlleur = new AjoutParfum();
        loader.setController(controlleur);
        try{
            stageAjoutParfum.setScene(new Scene(loader.load()));
            stageAjoutParfum.showAndWait();
        } catch (IOException e) {
            Alert alerte = new Alert(Alert.AlertType.ERROR, "Erreur lors du lancement de la fenêtre", ButtonType.OK);
            alerte.show();
        }

        return controlleur;
    }

    @FXML
    private void lancerFenetreAjoutHabit(){
        Stage secondStage = new Stage();
        secondStage.initOwner(listeProduit.getScene().getWindow());
        secondStage.initModality(Modality.WINDOW_MODAL);
        AjoutHabit controlleur = initAjouterHabit(secondStage);
        if(!controlleur.isCanceled()){
            boutiqueVM.ajouterHabit(controlleur.getNomHabit(), controlleur.getPrixHabit());
        }
    }

    private AjoutHabit initAjouterHabit(Stage stageAjoutHabit) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Ajout_Habit.fxml"));
        AjoutHabit controlleur = new AjoutHabit();
        loader.setController(controlleur);
        try{
            stageAjoutHabit.setScene(new Scene(loader.load()));
            stageAjoutHabit.showAndWait();
        } catch (IOException e) {
            Alert alerte = new Alert(Alert.AlertType.ERROR, "Erreur lors du lancement de la fenêtre", ButtonType.OK);
            alerte.show();
        }

        return controlleur;
    }*/

    @FXML
    private void supprimerSelectionne(){
        boutiqueVM.supprimerProduit(listeProduit.getSelectionModel().getSelectedItem());
    }
}
