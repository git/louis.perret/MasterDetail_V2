package view;

import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import view_modele.ProduitVM;

import java.util.ArrayList;
import java.util.List;

public class AjoutParfum {

    @FXML
    private Label fragrancesAjoutees;

    @FXML
    private TextField nomParfum;

    @FXML
    private TextField prixParfum;

    @FXML
    private TextField fragranceParfum;

    private final List<String> fragrances = new ArrayList<>();

    private boolean isCanceled = false;

    @FXML
    private void ajouterParfum() {
        fermerFenetre();
    }

    @FXML
    private void annuler() {
        isCanceled = true;
        fermerFenetre();
    }

    private void fermerFenetre(){
        fragranceParfum.getScene().getWindow().hide();
    }

    @FXML
    private void ajouterFragrance() {
        fragrances.add(fragranceParfum.getText());
        fragrancesAjoutees.textProperty().bind(Bindings.concat("Fragrances ajoutées : ", fragrancesToString()));
        fragranceParfum.setText("");
;    }

    private String fragrancesToString(){
        StringBuilder str = new StringBuilder();
        for(String s : fragrances){
            str.append(String.format("%s,",s));
        }

        return str.toString();
    }
    public String getNomParfum(){
        return nomParfum.getText();
    }

    public int getPrixParfum(){
        return Integer.parseInt(prixParfum.getText());
    }

    public List<String> getFragrancesParfum(){
        return fragrances;
    }

    public boolean isCanceled() {
        return isCanceled;
    }
}
