package view.cellules;

import javafx.scene.control.ListCell;
import view_modele.ProduitVM;

public class CelluleProduit extends ListCell<ProduitVM> {

    @Override
    protected void updateItem(ProduitVM item, boolean empty) {
        super.updateItem(item, empty);
        if(!empty){
            textProperty().bind(item.nomProperty());
        }
        else{
            textProperty().unbind();
            setText("");
        }
    }
}
