package view.cellules;

import javafx.scene.control.ListCell;
import view_modele.TailleVM;

public class CelluleTaille extends ListCell<TailleVM> {

    @Override
    protected void updateItem(TailleVM item, boolean empty) {
        super.updateItem(item, empty);
        if(!empty){
            setText(item.toString());
        }
        else{
            setText("");
        }
    }
}
