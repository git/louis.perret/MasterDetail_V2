package view.cellules;

import javafx.beans.binding.Bindings;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import view_modele.CouleurVM;


public class CelluleCouleur extends ListCell<CouleurVM> {

    private BorderPane layout = new BorderPane();

    private Rectangle rectangleCouleur;

    private Label couleur = new Label();

    @Override
    protected void updateItem(CouleurVM item, boolean empty) {
        super.updateItem(item, empty);
        if(!empty){
            rectangleCouleur = new Rectangle(30,10, Color.rgb(item.getRouge(), item.getVert(), item.getBleue()));
            couleur.textProperty().bind(Bindings.concat(item.rougeProperty(), ", ", item.vertProperty(), ", ", item.bleueProperty()));
            layout.setLeft(rectangleCouleur);
            layout.setCenter(couleur);
            setGraphic(layout);
        }
        else{
            rectangleCouleur = null;
            couleur.textProperty().unbind();
            setGraphic(null);
        }
    }
}
