package view.uc;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import view.cellules.CelluleCouleur;
import view.cellules.CelluleTaille;
import view_modele.CouleurVM;
import view_modele.HabitVM;
import view_modele.TailleVM;

import java.io.IOException;

public class UCDetailHabit extends VBox {

    @FXML
    private ListView<CouleurVM> listeCouleurs = new ListView<>();

    @FXML
    private HBox layout_UCCommun;

    @FXML
    private ColorPicker colorPicker;

    @FXML
    private ComboBox<TailleVM> comboTaille;

    @FXML
    private ListView<TailleVM> listeTailles = new ListView<>();

    private UCCommun uc_commun;

    private HabitVM habitVM;

    public UCDetailHabit() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/uc/UC_DetailHabit.fxml"));
        loader.setController(this);
        loader.setRoot(this);
        loader.load();

        uc_commun = new UCCommun();
        layout_UCCommun.getChildren().add(uc_commun);

        listeCouleurs.setCellFactory(__ -> new CelluleCouleur());
        listeTailles.setCellFactory(__ -> new CelluleTaille());
    }

    public void setHabitVM(HabitVM habitVM){
        this.habitVM = habitVM;
        bind();
    }

    private void bind(){
        unbind();
        uc_commun.bind(this.habitVM);
        listeCouleurs.itemsProperty().bind(this.habitVM.listeCouleursProperty());
        listeTailles.itemsProperty().bind(this.habitVM.listeTaillesProperty());
        comboTaille.setItems(habitVM.getToutesLesTailles());
    }

    private void unbind(){
        if(this.habitVM != null){
            uc_commun.unbind(habitVM);
            listeCouleurs.itemsProperty().unbind();
            listeTailles.itemsProperty().unbind();
            comboTaille.setItems(null);
        }
    }

    public void setVisibiliy(boolean visible){
        if(!visible){
            unbind();
        }
        super.setVisible(visible);
    }

    @FXML
    private void ajouterCouleur(){
        Color color = colorPicker.getValue();
        habitVM.ajouterCouleur((int)(color.getGreen()*255), (int)(color.getRed()*255), (int)(color.getBlue()*255));
    }

    @FXML
    private void supprimerCouleur(){
        habitVM.supprimerCouleur(listeCouleurs.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void deselectionner(){
        listeCouleurs.getSelectionModel().select(-1);
        listeTailles.getSelectionModel().select(-1);
    }

    @FXML
    private void ajouterTaille(){
        habitVM.ajouterTaille(comboTaille.getValue());
    }

    @FXML
    private void supprimerTaille(){
        habitVM.supprimerTaille(listeTailles.getSelectionModel().getSelectedItem());
    }


}
