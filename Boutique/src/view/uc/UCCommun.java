package view.uc;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.util.converter.NumberStringConverter;
import view_modele.ProduitVM;

import java.io.IOException;

public class UCCommun extends VBox {

    @FXML
    private TextField nomProduit;

    @FXML
    private TextField prixProduit;

    public UCCommun() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/uc/UC_Commun.fxml"));
        loader.setController(this);
        loader.setRoot(this);
        loader.load();
    }

    public void bind(ProduitVM produit){
        nomProduit.textProperty().bindBidirectional(produit.nomProperty());
        prixProduit.textProperty().bindBidirectional(produit.prixProperty(), new NumberStringConverter());
    }

    public void unbind(ProduitVM produit){
        nomProduit.textProperty().unbindBidirectional(produit.nomProperty());
        prixProduit.textProperty().unbindBidirectional(produit.prixProperty());
    }
}
