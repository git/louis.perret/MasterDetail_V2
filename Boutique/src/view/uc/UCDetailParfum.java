package view.uc;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import view_modele.ParfumVM;

import java.io.IOException;

public class UCDetailParfum extends VBox {

    private ParfumVM parfumVM;

    @FXML
    private ListView<String> listeFragrances;

    @FXML
    private TextField nouvelleFragrance;

    @FXML
    private HBox layout_UCCommun;

    private UCCommun ucCommun;

    public UCDetailParfum() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/uc/UC_DetailParfum.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        loader.load();

        ucCommun = new UCCommun();
        layout_UCCommun.getChildren().add(ucCommun);
    }

    public void setParfumVM(ParfumVM parfum){
        this.parfumVM = parfum;
        bind();
    }

    private void bind(){
        unbind();
        ucCommun.bind(this.parfumVM);
        listeFragrances.itemsProperty().bind(this.parfumVM.listeFragrancesProperty());
    }

    private void unbind(){
        if(this.parfumVM != null){
            ucCommun.unbind(parfumVM);
            listeFragrances.itemsProperty().unbind();
            nouvelleFragrance.setText("");
        }
    }

    public void setVisibiliy(boolean visible){
        if(!visible){
            unbind();
        }
        super.setVisible(visible);
    }

    @FXML
    private void ajouterFragrance(){
        parfumVM.addFragrance(nouvelleFragrance.getText());
        nouvelleFragrance.setText("");
    }

    @FXML
    private void supprimerFragrance(){
        parfumVM.removeFragrance(listeFragrances.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void deselectionner(){
        listeFragrances.getSelectionModel().select(-1);
    }


}
